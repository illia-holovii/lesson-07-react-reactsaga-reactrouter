const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const users = require('./users.json');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const app = express();

require('./passport.config');

app.use(express.static('.'));
app.use(passport.initialize());
app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, PATCH, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

app.get('/user', (req, res) => {
  fs.readFile('./userlist.json', 'utf-8', (err, data) => {
    res.send(data);
  });
});

app.get('/user/:id', (req, res) => {
  fs.readFile('./userlist.json', 'utf-8', (err, data) => {
    if (err) {
      res.send('{"result": 0}');
    } else {
      const id = req.params.id;
      const fighters = JSON.parse(data);
  
      if (!id) {
        res.send('{"result": 0}');
      }
  
      const newFighter = fighters.filter((fighter) => fighter._id === id);
  
      if (newFighter.length === 0) {
        res.send('{"result": 0}');
      } else {
        res.send(JSON.stringify(newFighter[0]));
      }
    }
  });
});

// app.post('/user', (req, res) => {
//   fs.readFile('./userlist.json', 'utf-8', (err, data) => {
//     if (err) {
//       res.send('{"result": 0}');
//     } else {
//       const fighters = JSON.parse(data);
//       const fighter = req.body;
//       let repeat = true;
  
//       fighters.forEach(element => {
//         if (element._id === fighter._id) repeat = false; 
//       });

//       if (repeat) {
//         fighters.push(fighter);

//         fs.writeFile('./userlist.json', JSON.stringify(fighters), (err) => {
//           if (err) {
//             res.send('{"result": 0}');
//           } else {
//             res.send('{"result": 1}');
//           }
//           console.log('done');
//         });
//       } else {
//         res.send('{"result": 0}');
//       }
//     }
//   });
// });

app.put('/user/:id', (req, res) => {
  fs.readFile('./userlist.json', 'utf-8', (err, data) => {
    if (err) {
      res.send('{"result": 0}');
    } else {
      const fighters = JSON.parse(data);
      const fighterUpdated = req.body;
      const id = req.params.id;

      if (fighterUpdated._id === id) {
        const newFighters = fighters.filter((fighter) => fighter._id !== id);
        newFighters.push(fighterUpdated);

        fs.writeFile('./userlist.json', JSON.stringify(newFighters), (err) => {
          if (err) {
            res.send('{"result": 0}');
          } else {
            res.send('{"result": 1}');
          }
        });
      } else {
        res.send('{"result": 0}');
      }
    }
  });
});

app.delete(`/user/:id`, (req, res) => {
  fs.readFile('./userlist.json', 'utf-8', (err, data) => {
    if (err) {
      res.send('{"result": 0}');
    } else {
      const fighters = JSON.parse(data);
      const id = req.params.id;
      
      if (!id) {
        res.send('{"result": 0}');
      }
  
      const newFighters = fighters.filter((fighter) => fighter._id !== id);

      if (newFighters.length === fighters.length) {
        res.send('{"result": 0}');
      } else {
        fs.writeFile('./userlist.json', JSON.stringify(newFighters), (err) => {
          if (err) {
            res.send('{"result": 0}');
          } else {
            res.send('{"result": 1}');
          }
        });
      }
    }
  });
});

app.post('/', (req, res) => {
  console.log("oye oye oye oye oye");
  const userFromReq = req.body;
  
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret', { expiresIn: '24h' });
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

app.listen(5000, function() {
  console.log('server is running on port 5000');
});