import React from 'react';
import UserList from './users/index';
import UserPage from './userPage/index';
import { Switch, Route } from 'react-router-dom';
import Login from './login/login';
import './App.css';
import Chat from './chat/Chat';

function App() {
	return (
		<div className="App">
			<Switch>
				<Route exact path='/' component={Login} />
				<Route exact path="/user" component={UserPage} />
				<Route path="/user/:id" component={UserPage} />
				<Route path="/chat" component={Chat} />
			</Switch>
		</div>
	);
}

export default App;