import React from 'react';
import './Header.css';

class Header extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="Header">
                <h1>My chat</h1>
                <div className="InfoCount">
                    <h3>8 users</h3>
                    <h3>12 messages</h3>
                </div>
                <span>last message at 12:43</span>
            </div>
        )
    }
}

export default Header;