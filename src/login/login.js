import React from 'react';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValueL: '',
      inputValueP: ''
    };
  }
    
  logIn() {
    console.log(this.state);
    fetch('/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        login: this.state.inputValueL,
        password: this.state.inputValueP
      })
    }).then(res => {
      res.json().then(body => {
        console.log(body);
        if (body.auth) {
          localStorage.setItem('jwt', body.token);
          this.props.history.push('/user');
        } else {
          console.log('auth failed');
        }
      })
    }).catch(err => {
      console.log('request went wrong');
    })
  }

  updateInputValueL(evt) {
    this.setState({
      inputValueL: evt.target.value
    });
  }

  updateInputValueP(evt) {
    this.setState({
      inputValueP: evt.target.value
    });
  }  

  render() {
    return (
      <div className="login-user">
        <input type="text" value={this.state.inputValueL} onChange={evt => this.updateInputValueL(evt)}/>
        <input type="password" value={this.state.inputValueP} onChange={evt => this.updateInputValueP(evt)}/>
        <button onClick={() => this.logIn()}>SEND</button>
      </div>
    );
  }
}

export default Login;